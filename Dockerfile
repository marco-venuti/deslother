FROM python:slim-bullseye

RUN apt-get update && apt-get install -yqq git ffmpeg \
    && rm -rf /var/lib/{apt,dpkg,cache,log}

RUN pip install pyyaml numpy python-decouple

RUN git clone https://github.com/marco-venuti/video-remove-silence.git /root/video-remove-silence

COPY deslother.py /root/

ENV PYTHONUNBUFFERED=0
