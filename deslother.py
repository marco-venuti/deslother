#!/usr/bin/env python3

from sys import argv
import os
import yaml
import subprocess
import numpy as np
import logging
from logging.handlers import SMTPHandler
from decouple import config

smtp_server = config('SMTP_SERVER')
mail_from = config('MAIL_FROM')
mail_to = config('MAIL_TO')
fmt = '[%(asctime)s] %(levelname)s: %(message)s'

logging.basicConfig(
    level=logging.INFO, format=fmt
)

mail_handler = SMTPHandler(
    mailhost=smtp_server.split(':'),
    fromaddr=f'Deslother <{mail_from}>',
    toaddrs=[mail_to],
    subject='[Deslother logs]'
)
mail_handler.setLevel(logging.ERROR)
mail_handler.setFormatter(logging.Formatter(fmt))
logging.getLogger().addHandler(mail_handler)

logging.info('Deslother started')


def strip_ext(path):
    return os.path.splitext(path)[0]


VIDEO_FORMATS = ('.mp4', '.mkv')

conf_path = argv[1]

if not os.path.exists(conf_path):
    exit(1)

with open(conf_path) as stream:
    try:
        conf = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        logging.error(exc)
        exit(1)

paths = conf['paths']

for path in paths:
    logging.info(f'Starting path {path}')

    path_d = os.path.join('/mnt', path)

    if not os.path.exists(path_d):
        logging.error(f'Path {path} does not exist')
        exit(1)

    nosloth_path = os.path.join(path_d, 'nosloth')
    try:
        os.mkdir(nosloth_path)
    except FileExistsError:
        pass

    files_orig = list(filter(
        lambda x: x.endswith(VIDEO_FORMATS),
        os.listdir(path_d)
    ))

    files_done = os.listdir(nosloth_path)
    files_done_noext = list(map(strip_ext, files_done))

    mask = [
        strip_ext(x) not in files_done_noext
        for x in files_orig
    ]

    files_todo = np.array(files_orig)[mask]

    if files_todo.size == 0:
        logging.info(f'Nothing to do for path {path}')

    for file_todo in sorted(files_todo):
        logging.info(f'Starting to desloth {path}/{file_todo}')
        p = subprocess.run(
            [
                '/root/video-remove-silence/video-remove-silence',
                os.path.join(path_d, file_todo)
            ]
        )
        if p.returncode != 0:
            logging.error(f'video-remove-silence failed on {path}/{file_todo}')
            exit(1)

        os.rename(
            os.path.join(path_d, strip_ext(file_todo) + '_result.mp4'),
            os.path.join(nosloth_path, strip_ext(file_todo) + '.mp4')
        )
        logging.info(f'Deslothed {path}/{file_todo}')
